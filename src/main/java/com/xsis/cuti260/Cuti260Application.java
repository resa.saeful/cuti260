package com.xsis.cuti260;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Cuti260Application {

	public static void main(String[] args) {
		SpringApplication.run(Cuti260Application.class, args);
	}

}

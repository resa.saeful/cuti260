package com.xsis.cuti260.controllers;


import com.xsis.cuti260.models.Status;
import com.xsis.cuti260.repositories.StatusRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiStatusController {
    @Autowired
    private StatusRepo statusRepo;

    @GetMapping("/status")
    public ResponseEntity<List<Status>> GetAllStatus() {
        try {
            List<Status> status = this.statusRepo.findAll();
            return new ResponseEntity<>(status, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("status/{id}")
    public ResponseEntity<List<Status>> GetStatusById(@PathVariable("id")Long id)
    {
        try
        {
            Optional<Status> status = this.statusRepo.findById(id);
            if (status.isPresent())
            {
                ResponseEntity rest = new ResponseEntity(status, HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }



    @PostMapping(value = "/status")
    public ResponseEntity<Object> saveCategory(@RequestBody Status status)
    {
        try {
            status.setCreatedBy("Resa");
            status.setCreatedOn(new Date());
            this.statusRepo.save(status);
            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/status/{id}")
    public ResponseEntity<Object> UpdateCategory(@RequestBody Status status, @PathVariable("id") Long id)
    {
        try {
            Optional<Status> statusData = this.statusRepo.findById(id);

            if (statusData.isPresent())
            {
                status.setId(id);
                status.setModifiedBy("resa");
                status.setModifiedOn(new Date());
                this.statusRepo.save(status);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/status/{id}")
    public ResponseEntity<Object> DeleteStatus(@RequestBody Status status, @PathVariable("id") Long id)
    {
        try {
            Optional<Status> statusData = this.statusRepo.findById(id);

            if (statusData.isPresent())
            {
                status.setId(id);
                status.setModifiedBy("Resa");
                status.setModifiedOn(new Date());
                status.setDelete(true);
                this.statusRepo.save(status);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }
}

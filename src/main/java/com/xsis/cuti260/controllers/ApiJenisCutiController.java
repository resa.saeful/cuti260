package com.xsis.cuti260.controllers;


import com.xsis.cuti260.models.JenisCuti;
import com.xsis.cuti260.repositories.JenisCutiRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiJenisCutiController {

    @Autowired
    private JenisCutiRepo jenisCutiRepo;

    @GetMapping("/jenis_cuti")
    public ResponseEntity<List<JenisCuti>> GetAllJenisCuti() {
        try {
            List<JenisCuti> jenisCuti = this.jenisCutiRepo.findAll();
            return new ResponseEntity<>(jenisCuti, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("jenis_cuti/{id}")
    public ResponseEntity<List<JenisCuti>> GetJenisCutiById(@PathVariable("id")Long id)
    {
        try
        {
            Optional<JenisCuti> jenisCuti = this.jenisCutiRepo.findById(id);
            if (jenisCuti.isPresent())
            {
                ResponseEntity rest = new ResponseEntity(jenisCuti, HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }



    @PostMapping(value = "/jenis_cuti")
    public ResponseEntity<Object> SaveJenisCuti(@RequestBody JenisCuti jenisCuti)
    {
        try {
            jenisCuti.setCreatedBy("Resa");
            jenisCuti.setCreatedOn(new Date());
            this.jenisCutiRepo.save(jenisCuti);
            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/jenis_cuti/{id}")
    public ResponseEntity<Object> UpdateJenisCuti(@RequestBody JenisCuti jenisCuti, @PathVariable("id") Long id)
    {
        try {
            Optional<JenisCuti> jenisCutiData = this.jenisCutiRepo.findById(id);

            if (jenisCutiData.isPresent())
            {
                jenisCuti.setId(id);
                jenisCuti.setModifiedBy("resa");
                jenisCuti.setModifiedOn(new Date());
                this.jenisCutiRepo.save(jenisCuti);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/jenis_cuti/{id}")
    public ResponseEntity<Object> DeleteJenisCuti(@RequestBody JenisCuti jenisCuti, @PathVariable("id") Long id)
    {
        try {
            Optional<JenisCuti> jenisCutiData = this.jenisCutiRepo.findById(id);

            if (jenisCutiData.isPresent())
            {
                jenisCuti.setId(id);
                jenisCuti.setModifiedBy("Resa");
                jenisCuti.setModifiedOn(new Date());
                jenisCuti.setDelete(true);
                this.jenisCutiRepo.save(jenisCuti);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }
}

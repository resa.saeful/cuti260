package com.xsis.cuti260.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/jenis_karyawan/")
public class JenisKaryawanController {
    @GetMapping(value = "index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("jenis_karyawan/index");
        return view;
    }
}

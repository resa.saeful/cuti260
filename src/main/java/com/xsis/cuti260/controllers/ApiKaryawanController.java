package com.xsis.cuti260.controllers;


import com.xsis.cuti260.models.Karyawan;
import com.xsis.cuti260.models.Login;
import com.xsis.cuti260.repositories.KaryawanRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiKaryawanController {


    @Autowired
    private KaryawanRepo karyawanRepo;

    @GetMapping("/karyawan")
    public ResponseEntity<List<Karyawan>> GetAllKaryawan() {
        try {
            List<Karyawan> karyawan = this.karyawanRepo.findAll();
            return new ResponseEntity<>(karyawan, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("karyawan/{id}")
    public ResponseEntity<List<Karyawan>> GetKaryawanById(@PathVariable("id")Long id)
    {
        try
        {
            Optional<Karyawan> karyawan = this.karyawanRepo.findById(id);
            if (karyawan.isPresent())
            {
                ResponseEntity rest = new ResponseEntity(karyawan, HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }



    @PostMapping(value = "/karyawan")
    public ResponseEntity<Object> saveCategory(@RequestBody Karyawan karyawan)
    {
        try {
            karyawan.setCreatedBy("Resa");
            karyawan.setCreatedOn(new Date());
            this.karyawanRepo.save(karyawan);
            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/karyawan/{id}")
    public ResponseEntity<Object> UpdateKaryawan(@RequestBody Karyawan karyawan, @PathVariable("id") Long id)
    {
        try {
            Optional<Karyawan> karyawanData = this.karyawanRepo.findById(id);

            if (karyawanData.isPresent())
            {
                karyawan.setId(id);
                karyawan.setModifiedBy("resa");
                karyawan.setModifiedOn(new Date());
                this.karyawanRepo.save(karyawan);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/karyawan/{id}")
    public ResponseEntity<Object> DeleteKaryawan(@RequestBody Karyawan karyawan, @PathVariable("id") Long id)
    {
        try {
            Optional<Karyawan> karyawanData = this.karyawanRepo.findById(id);

            if (karyawanData.isPresent())
            {
                karyawan.setId(id);
                karyawan.setModifiedBy("Resa");
                karyawan.setModifiedOn(new Date());
                karyawan.setDelete(true);
                this.karyawanRepo.save(karyawan);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/validate")
    public ResponseEntity<Object> ValidateKaryawan(@RequestBody Login login, HttpSession session) {
        try {
            Optional<Karyawan> karyawanData = this.karyawanRepo.findByEmailPassword(login.getEmail(), login.getPassword());
            if (karyawanData.isPresent()) {
                session.setAttribute("id", karyawanData.get().getId());
                session.setAttribute("jenisKaryawanId", karyawanData.get().getJenisKaryawanId());
                session.setAttribute("nama", karyawanData.get().getNama());
                return new ResponseEntity<>("success", HttpStatus.OK);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }


}

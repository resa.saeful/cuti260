package com.xsis.cuti260.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/cuti/")
public class CutiController {
    @GetMapping(value = "index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("cuti/index");
        return view;
    }    
}

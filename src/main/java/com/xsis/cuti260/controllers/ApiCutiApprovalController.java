package com.xsis.cuti260.controllers;

import java.util.*;

import com.xsis.cuti260.models.CutiApproval;
import com.xsis.cuti260.repositories.CutiApprovalRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiCutiApprovalController {

    @Autowired
    private CutiApprovalRepo cutiApprovalRepo;

    @GetMapping("/cuti_approval")
    public ResponseEntity<List<CutiApproval>> GetAllCutiApproval() {
        try {
            List<CutiApproval> cuti = this.cutiApprovalRepo.findAll();
            return new ResponseEntity<>(cuti, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("cuti_approval/{id}")
    public ResponseEntity<List<CutiApproval>> GetCutiApprovalById(@PathVariable("id")Long id)
    {
        try
        {
            Optional<CutiApproval> cuti = this.cutiApprovalRepo.findById(id);
            if (cuti.isPresent())
            {
                ResponseEntity rest = new ResponseEntity(cuti, HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping(value = "/cuti_approval")
    public ResponseEntity<Object> SaveCutiApproval(@RequestBody CutiApproval cutiApproval)
    {
        try {
            cutiApproval.setCreatedBy("Resa");
            cutiApproval.setCreatedOn(new Date());
            this.cutiApprovalRepo.save(cutiApproval);
            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/cuti_approval/{id}")
    public ResponseEntity<Object> UpdateCutiApproval(@RequestBody CutiApproval cutiApproval, @PathVariable("id") Long id)
    {
        try {
            Optional<CutiApproval> cutiApprovalData = this.cutiApprovalRepo.findById(id);

            if (cutiApprovalData.isPresent())
            {
                cutiApproval.setId(id);
                cutiApproval.setModifiedBy("resa");
                cutiApproval.setModifiedOn(new Date());
                this.cutiApprovalRepo.save(cutiApproval);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/cuti_approval/{id}")
    public ResponseEntity<Object> DeleteCutiApproval(@RequestBody CutiApproval cutiApproval, @PathVariable("id") Long id)
    {
        try {
            Optional<CutiApproval> cutiApprovalData = this.cutiApprovalRepo.findById(id);

            if (cutiApprovalData.isPresent())
            {
                cutiApproval.setId(id);
                cutiApproval.setModifiedBy("Resa");
                cutiApproval.setModifiedOn(new Date());
                cutiApproval.setDelete(true);
                this.cutiApprovalRepo.save(cutiApproval);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }
}

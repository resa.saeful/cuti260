package com.xsis.cuti260.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.xsis.cuti260.models.Cuti;
import com.xsis.cuti260.repositories.CutiRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiCutiController {
    @Autowired
    private CutiRepo cutiRepo;

    @GetMapping("/cuti")
    public ResponseEntity<List<Cuti>> GetAllCuti() {
        try {
            List<Cuti> cuti = this.cutiRepo.findAll();
            return new ResponseEntity<>(cuti, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("cuti/{id}")
    public ResponseEntity<List<Cuti>> GetCutiById(@PathVariable("id")Long id)
    {
        try
        {
            Optional<Cuti> cuti = this.cutiRepo.findById(id);
            if (cuti.isPresent())
            {
                ResponseEntity rest = new ResponseEntity(cuti, HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping(value = "/cuti")
    public ResponseEntity<Object> SaveCuti(@RequestBody Cuti cuti)
    {
        try {
            cuti.setCreatedBy("Resa");
            cuti.setCreatedOn(new Date());
            this.cutiRepo.save(cuti);
            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/cuti/{id}")
    public ResponseEntity<Object> UpdateCuti(@RequestBody Cuti cuti, @PathVariable("id") Long id)
    {
        try {
            Optional<Cuti> cutiData = this.cutiRepo.findById(id);

            if (cutiData.isPresent())
            {
                cuti.setId(id);
                cuti.setModifiedBy("resa");
                cuti.setModifiedOn(new Date());
                this.cutiRepo.save(cuti);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/cuti/{id}")
    public ResponseEntity<Object> DeleteCuti(@RequestBody Cuti cuti, @PathVariable("id") Long id)
    {
        try {
            Optional<Cuti> cutiData = this.cutiRepo.findById(id);

            if (cutiData.isPresent())
            {
                cuti.setId(id);
                cuti.setModifiedBy("Resa");
                cuti.setModifiedOn(new Date());
                cuti.setDelete(true);
                this.cutiRepo.save(cuti);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }
    
}

package com.xsis.cuti260.controllers;


import com.xsis.cuti260.models.JenisKaryawan;
import com.xsis.cuti260.repositories.JenisKaryawanRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiJenisKaryawanController {
    @Autowired
    private JenisKaryawanRepo jenisKaryawanRepo;

    @GetMapping("/jenis_karyawan")
    public ResponseEntity<List<JenisKaryawan>> GetAllJenisKaryawan() {
        try {
            List<JenisKaryawan> jenisKaryawan = this.jenisKaryawanRepo.findAll();
            return new ResponseEntity<>(jenisKaryawan, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("jenis_karyawan/{id}")
    public ResponseEntity<List<JenisKaryawan>> GetJenisKaryawanById(@PathVariable("id")Long id)
    {
        try
        {
            Optional<JenisKaryawan> jenisKaryawan = this.jenisKaryawanRepo.findById(id);
            if (jenisKaryawan.isPresent())
            {
                ResponseEntity rest = new ResponseEntity(jenisKaryawan, HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }



    @PostMapping(value = "/jenis_karyawan")
    public ResponseEntity<Object> saveCategory(@RequestBody JenisKaryawan jenisKaryawan)
    {
        try {
            jenisKaryawan.setCreatedBy("Resa");
            jenisKaryawan.setCreatedOn(new Date());
            this.jenisKaryawanRepo.save(jenisKaryawan);
            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/jenis_karyawan/{id}")
    public ResponseEntity<Object> UpdateCategory(@RequestBody JenisKaryawan jenisKaryawan, @PathVariable("id") Long id)
    {
        try {
            Optional<JenisKaryawan> jenisKaryawanData = this.jenisKaryawanRepo.findById(id);

            if (jenisKaryawanData.isPresent())
            {
                jenisKaryawan.setId(id);
                jenisKaryawan.setModifiedBy("resa");
                jenisKaryawan.setModifiedOn(new Date());
                this.jenisKaryawanRepo.save(jenisKaryawan);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/jenis_karyawan/{id}")
    public ResponseEntity<Object> DeleteJenisKaryawan(@RequestBody JenisKaryawan jenisKaryawan, @PathVariable("id") Long id)
    {
        try {
            Optional<JenisKaryawan> jenisKaryawanData = this.jenisKaryawanRepo.findById(id);

            if (jenisKaryawanData.isPresent())
            {
                jenisKaryawan.setId(id);
                jenisKaryawan.setModifiedBy("Resa");
                jenisKaryawan.setModifiedOn(new Date());
                jenisKaryawan.setDelete(true);
                this.jenisKaryawanRepo.save(jenisKaryawan);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }
}

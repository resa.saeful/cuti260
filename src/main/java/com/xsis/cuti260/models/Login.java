package com.xsis.cuti260.models;

import javax.persistence.*;

@Entity
@Table(name = "login")
public class Login {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;
    
    @Column(name = "email")
    private String Email;

    @Column(name = "password")
    private String Password;

    // public Login() {

    // }

    // public Login(Long id, String email, String password) {
		
	// 	this.Id = id;
	// 	this.Email = email;
	// 	this.Password = password;
	// }
    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    
}

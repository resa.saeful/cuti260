package com.xsis.cuti260.models;


import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Where(clause = "is_delete = false")
@Table(name = "status")
public class Status extends Common{
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @Column(name = "status")
    private String Status;

    @Column(name = "keterangan")
    private String Keterangan;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getKeterangan() {
        return Keterangan;
    }

    public void setKeterangan(String keterangan) {
        Keterangan = keterangan;
    }
}

package com.xsis.cuti260.models;

import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.hibernate.annotations.Where;

@Entity
@Where(clause = "is_delete = false")
@Table (name = "cuti")
public class Cuti extends Common{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "jenis_cuti_id", insertable = false, updatable = false)
    public JenisCuti jenisCuti;

    @Column(name = "jenis_cuti_id", nullable = true)
    private Long JenisCutiId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    @Column(name = "tanggal_awal")
    private Date TanggalAwal;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    @Column(name = "tanggal_akhir")
    private Date TanggalAkhir;

    @Column(name = "lama_cuti")
    private Long LamaCuti;

    @ManyToOne
    @JoinColumn(name = "status_id", insertable = false, updatable = false)
    public Status status;

    @Column(name = "status_id", nullable = true)
    private Long StatusId = 1L;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public JenisCuti getJenisCuti() {
        return jenisCuti;
    }

    public void setJenisCuti(JenisCuti jenisCuti) {
        this.jenisCuti = jenisCuti;
    }

    public Long getJenisCutiId() {
        return JenisCutiId;
    }

    public void setJenisCutiId(Long jenisCutiId) {
        JenisCutiId = jenisCutiId;
    }

    public Date getTanggalAwal() {
        return TanggalAwal;
    }

    public void setTanggalAwal(Date tanggalAwal) {
        TanggalAwal = tanggalAwal;
    }

    public Date getTanggalAkhir() {
        return TanggalAkhir;
    }

    public void setTanggalAkhir(Date tanggalAkhir) {
        TanggalAkhir = tanggalAkhir;
    }

    public Long getLamaCuti() {
        return LamaCuti ;
    }

    public void setLamaCuti(Long lamaCuti) {
        LamaCuti = lamaCuti;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getStatusId() {
        return StatusId;
    }

    public void setStatusId(Long statusId) {
        StatusId = statusId;
    }

    

    
}

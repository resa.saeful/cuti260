package com.xsis.cuti260.models;

import javax.persistence.*;

import org.hibernate.annotations.Where;

@Entity
@Where(clause = "is_delete = false")
@Table (name = "cuti_approval")
public class CutiApproval extends Common{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "cuti_id", insertable = false, updatable = false)
    public Cuti cuti;

    @Column(name = "cuti_id", nullable = true)
    private Long CutiId;

    @ManyToOne
    @JoinColumn(name = "status_id", insertable = false, updatable = false)
    public Status status;

    @Column(name = "status_id", nullable = true)
    private Long StatusId;

    @Column(name = "keterangan", nullable = false)
    private String Keterangan;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Cuti getCuti() {
        return cuti;
    }

    public void setCuti(Cuti cuti) {
        this.cuti = cuti;
    }

    public Long getCutiId() {
        return CutiId;
    }

    public void setCutiId(Long cutiId) {
        CutiId = cutiId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getStatusId() {
        return StatusId;
    }

    public void setStatusId(Long statusId) {
        StatusId = statusId;
    }

    public String getKeterangan() {
        return Keterangan;
    }

    public void setKeterangan(String keterangan) {
        Keterangan = keterangan;
    }

    
}

package com.xsis.cuti260.repositories;

import java.util.List;

import com.xsis.cuti260.models.Cuti;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CutiRepo extends JpaRepository<Cuti, Long>{
    @Query("FROM Cuti WHERE JenisCutiId = ?1")
    List<Cuti> findByJenisCutiId(Long jenisCutiId);
}

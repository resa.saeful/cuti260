package com.xsis.cuti260.repositories;

import java.util.List;

import com.xsis.cuti260.models.Login;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LoginRepo extends JpaRepository<Login, Long> {
    //Login findByEmailAndPassword(String email, String password);
    @Query("FROM Karyawan WHERE Email = ?1 AND Password= ?1")
    List<Login> findByEmailAndPassword(String email, String password);
}

package com.xsis.cuti260.repositories;

import com.xsis.cuti260.models.Status;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusRepo extends JpaRepository<Status, Long> {
}

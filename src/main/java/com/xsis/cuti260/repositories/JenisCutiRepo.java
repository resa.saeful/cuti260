package com.xsis.cuti260.repositories;

import com.xsis.cuti260.models.JenisCuti;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JenisCutiRepo extends JpaRepository<JenisCuti, Long> {
}

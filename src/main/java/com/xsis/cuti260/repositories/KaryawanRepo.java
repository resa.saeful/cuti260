package com.xsis.cuti260.repositories;

import java.util.List;
import java.util.Optional;

import com.xsis.cuti260.models.Karyawan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface KaryawanRepo extends JpaRepository<Karyawan, Long> {
    @Query(value = "SELECT * FROM Karyawan e WHERE e.email = ?1 AND e.password = ?2", nativeQuery = true)       // using @query
    Optional<Karyawan> findByEmailPassword(String email, String password);

}

package com.xsis.cuti260.repositories;

import java.util.List;

import com.xsis.cuti260.models.CutiApproval;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CutiApprovalRepo extends JpaRepository<CutiApproval, Long>{
    @Query("FROM CutiApproval WHERE CutiId = ?1")
    List<CutiApproval> findByCutiId(Long cutiId);

    @Query("FROM CutiApproval WHERE StatusId = ?1")
    List<CutiApproval> findByStatusId(Long statusId);
}

package com.xsis.cuti260.repositories;

import com.xsis.cuti260.models.JenisKaryawan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JenisKaryawanRepo extends JpaRepository<JenisKaryawan, Long> {
}
